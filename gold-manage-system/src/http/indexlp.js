import axios from 'axios';

const http = axios.create({
  baseURL: 'http://172.16.110.253:8080/',
  timeout: 3000,
  // Content-Type: application/json
})

// 请求拦截
http.interceptors.request.use(config => {
  //如果有token 设置请求头
  if(localStorage.getItem('token')){
    config.headers.Authorization = localStorage.getItem('token');
  }
  return config;
},
error => {
  return Promise.reject(error);
})

//响应拦截
http.interceptors.response.use(response => {
  //更新token
  if (response.headers.authorization) {
    localStorage.setItem('token', response.headers.authorization);
  }
  return response;
},error => {
  return Promise.reject(error)
})

export default http;