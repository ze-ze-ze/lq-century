import http from '@/http/index'

// 增加总部
export const addCorpApi = (obj) => http({
  url: '/corp/addCorp',
  method: 'POST',
  params: {
    area: obj.district,//区县
    city: obj.city,//城市
    person: obj.contactPerson,//联系人
    personFax: obj.faxed,//传真
    personPhone: obj.telephone,//联系人电话
    province: obj.prov,//省份
    yingYeZhuangTai: obj.storeStatus == 'true' ? true : false,//营业状态
    zongBuId: obj.headquartersCode,//总部编号
    zongBuName: obj.headquartersName,//总部名称
    zongBuSite: obj.address,//总部地址
    zongBuArea: obj.region//总部区域
  }
});

// //查询总部
// export const getCorpApi = (zongBuId) => http({
//   url: '/corp/selectCorp',
//   method: 'POST',
//   params: {
//     zongBuId
//   }
// })

//删除总部
export const delCorpApi = (zongBuId) => http({
  url: '/corp/deleteCorp',
  method: 'POST',
  params: {
    zongBuId
  }
})

//修改总部
export const updateCorpApi = (obj) => http({
  url: '/corp/updataCorp',
  method: 'POST',
  params: {
    area: obj.district,//区县
    city: obj.city,//城市
    person: obj.contactPerson,//联系人
    personFax: obj.faxed,//传真
    personPhone: obj.telephone,//联系人电话
    province: obj.prov,//省份
    yingYeZhuangTai: obj.storeStatus == 'true' ? true : false,//营业状态
    zongBuId: obj.headquartersCode,//总部编号
    zongBuName: obj.headquartersName,//总部名称
    zongBuSite: obj.address,//总部地址
    zongBuArea: obj.region//总部区域
  }
})


//添加分店
export const addShopApi = (obj) => http({
  url: '/corp/addShop',
  method: 'POST',
  params: {
    area: obj.district,//区县
    city: obj.city,//城市
    fendianid: obj.branchCode,//分店编号
    fendianname: obj.nameOfStore,//分店名称
    fengbuarea: obj.region,//分店区域
    fengbucontact: obj.contactPerson,//联系人
    fengbufax: obj.faxed,//传真
    fengbufloor: obj.floor,//楼层
    fengbuphone: obj.telephone,//联系人电话
    fengbusite: obj.address,//分店地址
    province: obj.prov,//省份
    suoshucorp: obj.headquartersName,//所属总部
    yingyezhaungtai: obj.storeStatus == 'true' ? true : false,//营业状态
  }
})

// //查询分店
// export const getShopApi = (fendianid) => http({
//   url: '/corp/selectShop',
//   method: 'POST',
//   params: {
//     fendianid
//   }
// })

//删除分店
export const delShopApi = (fendianid) => http({
  url: '/corp/deleteShop',
  method: 'POST',
  params: {
    fendianid: Number(fendianid)
  }
})

//修改分店
export const updateShopApi = (obj) => http({
  url: '/corp/updataShop',
  method: 'POST',
  params: {
    area: obj.district,//区县
    city: obj.city,//城市
    fendianid: obj.branchCode,//分店编号
    fendianname: obj.nameOfStore,//分店名称
    fengbuarea: obj.region,//分店区域
    fengbucontact: obj.contactPerson,//联系人
    fengbufax: obj.faxed,//传真
    fengbufloor: obj.floor,//楼层
    fengbuphone: obj.telephone,//联系人电话
    fengbusite: obj.address,//分店地址
    province: obj.prov,//省份
    suoshucorp: obj.headquartersName,//所属总部
    yingyezhaungtai: obj.storeStatus == 'true' ? true : false,//营业状态
  }
})

// //便利总部信息
// export const getCorpListApi = () => http({
//   url: '/corp/selectCorpAll',
//   method: 'POST'
// })

//查询柜台信息
export const Counterinformation = (guitaiid) => http({
  url: '/corp/selectCounter',
  method: 'post',
  params: {
    guitaiid
  }
})

//遍历总部信息
export const selectCorpAll = () => http({
  url: '/corp/selectCorpAll',
  method: 'post',
})

//查询总部信息
export const selectCorpApi = (zongBuId) => http({
  url: 'corp/selectCorp',
  method: 'post',
  params: {
    zongBuId
  }
})

//查询分店信息
export const selectShopApi = (fendianid) => http({
  url: 'corp/selectShop',
  method: 'post',
  params: {
    fendianid
  }
})
//修改柜台信息
export const Modifycounter = (obj) => http({
  url: 'corp/updataCounter',
  method: 'post',
  params:
    obj

})
//增加柜台
export const addCounter = (addobj) => http({
  url: 'corp/addCounter',
  method: 'post',
  params:
    addobj
})

//删除柜台信息
export const deleteCounter = (guitaiid) => http({
  url: 'corp/deleteCounter',
  method: 'post',
  params: {
    guitaiid
  }
})