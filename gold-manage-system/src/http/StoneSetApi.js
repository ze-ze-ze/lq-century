import http from '@/http/index'

export const typeApi = () => http({
  url:'/stoneType/selectAll',
  method:'post',
})

export const updateApi = (options) => http({
  url:'/stoneType/update',
  method:'post',
  params:{
    code:options.code,
    company:options.company,
    id:options.id,
    name:options.name
  }
})

export const delApi = (id) => http({
  url:'/stoneType/deleteById',
  mthod:'post',
  params:{
    id
  }
})

export const delArrApi = (arr) => http({
  url:'/stoneType/deleteByIdArr',
  mehtod:'post',
  params:{
    arr
  }
})