import http from '@/http/index'

// 列表数据
export const getDoosList = (currentPage, pageSize) => http({
  url: '/outWareHouse/selectByPage',
  // url: '/merchandiseManagement/selectBypage',
  method: 'POST',
  params: {
    page: currentPage,
    size: pageSize
  }
});

// 获取列表总数量
// export const getDoosTotal = () => http({
//   url: '/activity/total',
//   method: 'GET'
// });

// 删除
export const delDoosApi = (id) => http({
  url: '/outWareHouse/delete',
  method: 'POST',
  params: {
    id: Number(id)
  }
})

// //修改审批
export const UpdateDoosApi = (obj) => http({
  url: '/outWareHouse/update',
  method: 'POST',
  params: {
    state: obj.state,
    id: obj.id
  }
})
//撤销驳回
export const cxBoHuiDoosApi = (obj, id) => http({
  url: '/outWareHouse/update',
  method: 'POST',
  params: {
    state: obj.state,
    id: id
  }
})

//模糊查询
export const getDoosDetail = (obj) => http({
  url: '/merchandiseManagement/selectByChoose',
  method: 'POST',
  params: {
    obj
  }
})

//商品出库新增
export const addDoosApi = (obj) => http({
  url: '/outWareHouse/insert',
  method: 'POST',
  params: {
    createperson: obj.createperson,//创单人
    counterid: obj.counterid,//关联柜台表的id
    createtime: obj.createtime,//创单时间
    documentid: obj.documentid,//单据编号
    from: obj.from,//来源
    idArr: obj.idArr,//商品id  id数组
    numbers: obj.numbers,//商品数量 数组
    orderid: obj.orderid,//订单id
    shopid: obj.shopid,//店铺id  关联门店表id
    splxid: obj.splxid,//商品类型id 关联商品类型表id
    supplierid: obj.supplierid,//供应商id 关联供应商表id
  }
})