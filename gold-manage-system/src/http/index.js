import axios from 'axios';
import {Loading} from 'element-ui';

const http = axios.create({
  baseURL: 'http://81.68.254.242:8080',
  timeout: 3000,
  // withCredentials: true,
})


// 请求拦截
http.interceptors.request.use(config => {
  //如果有token 设置请求头
  if (localStorage.getItem('token')) {
    config.headers.Authorization = localStorage.getItem('token');
  }
  // 请求前添加loading
  Loading.service({
    lock:true,
    text:'Loading',
    spinner:'el-icon-loading',
    background: 'rgba(0, 0, 0, 0.7)'
  })
  return config;
},
  error => {
    return Promise.reject(error);
  })

//响应拦截
http.interceptors.response.use(response => {
  //更新token
  if (response.headers.authorization) {
    localStorage.setItem('token', response.headers.authorization);
  }
  //关闭loading
  Loading.service().close()
  return response;
}, error => {
  //关闭loading
  Loading.service().close()
  return Promise.reject(error)
})

export default http;