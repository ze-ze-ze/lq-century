import http from './';

// 获取列表
export const getAllList = () => http({
  url: "/fixedCodeWare/selectByChoose",
  method: "post",
})

// 删除
export const deleteById = (id) => http({
  url: "/fixedCodeWare/deleteById",
  method: "post",
  params: {
    id
  }
})

// 修改状态
export const updateStatus = (id, state, userName) => http({
  url: "/fixedCodeWare/updateStates",
  method: "post",
  params: {
    id,
    state,
    userName
  }
})

// 新增
export const addInfos = (data) => http({
  url: "/fixedCodeWare/insert",
  method: "post",
  params: data
})