import http from "@/http/index";

//获取分页
export const returngood = (limit,currentPage) => http({
    url:'/purchase/selectBypage',
    method:'post',
    params:{
      limit:limit,
      page:currentPage
    }
  })
//获取所有数据
export const returngoodall = () => http({
    url:'purchase/selectAll',
    method:'post'
  })


//删除单行数据
export const deleteCai =(id)=> http({
  url:'purchase/deleteCai',
  method:'post',
  params:{
    id
  }
})
//模糊查询
export const selectByChoose =(limit,page,documentStartId)=>http({
  url:'purchase/selectByChoose',
  method:'post',
  params:{
    limit,
    page,
    documentStartId
  }
})

//状态编辑
export const updateState =()=>({
  url:'/purchase/updateState',
  method:'post',
})