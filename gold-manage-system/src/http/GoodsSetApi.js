import http from '@/http/index';
import Qs from 'qs'

//种类
export const typeApi = () => http({
  url:'/stoneType/selectAll',
  method:'post',
})

export const typeUpdateApi = (options) => http({
  url:'/stoneType/update',
  method:'post',
  params:{
    code:options.code,
    company:options.company,
    id:options.id,
    name:options.name
  }
})

export const typeDelApi = (id) => http({
  url:'/stoneType/deleteById',
  mthod:'post',
  // params:Qs.stringify({id})
  params:{
    id
  }
})

export const typeDelArrApi = (idArr) => http({
  url:'/stoneType/deleteByIdArr',
  mehtod:'POST',
  params: Qs.stringify({idArr},{arrayFormat:'repeat'})
})


//产品编码

export const getCodeApi = () => http({
  url:'/product/selectAll',
  method:'post'
})

export const codeDelApi = (id) => http({
  url:'product/delete',
  method:'post',
  params:{
    id
  }
})

export const insertApi = async (params) => await http({
  url:'/product/insert',
  mehtod:'post',
  params
})

export const serchApi = (options) =>  http({
  url:'/product/selectLike',
  mehtod:'post',
  params:options
})