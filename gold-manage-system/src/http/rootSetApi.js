import http from "./index";
import qs from "qs";

// 获取角色列表
export const getRoleList = () => http({
  url: "/role/selectRoles",
  method: "post"
})

// 新增角色
export const addRole = (name, description) => http({
  url: "/role/add",
  method: "post",
  params: {
    name,
    description
  }
})

// 修改角色
export const editTheRole = (data) => http({
  url: "/role/update",
  method: "post",
  params: data
})

// 删除角色
export const delTheRole = (id) => http({
  url: "/role/delete",
  method: "post",
  params: {
    id: id
  }
})

// 获取角色权限
export const getRolePermission = (id) => http({
  url: "/role/selectpurchase",
  method: "post",
  params: {
    id
  }
})

// 添加角色权限
export const addpurchase = function (id, params) {
  params = qs.stringify({ integers: params }, { arrayFormat: "repeat" })
  return () => http({
    url: "/role/addpurchase?id=" + id + "&" + params,
    method: "post",
  })
}