import http from './index'

// 获取数据渲染页码
export const getPurchaseTOtal = (limit, page) => http({
    url: '/purchase/selectBypage',
    method: 'post',
    params: {
        limit, page
    }
})
// 获取列表数据
export const getPurchaseListAPI = () => http({
    url: '/purchase/selectAll',
    method: 'post'
})
// 删除单个采购退货
export const getPurchaseDelAPI = (id) => http({
    url: '/purchase/deleteCai',
    method: 'POST',
    params: {
        id
    }
})
// 新增单个退货信息
export const addSingleListAPI = (object) => http({
    url: '/purchase/insertCai',
    method: 'POST',
    params: {
        object
    }

})
// 获取单个查看采购退货信息
export const getPurchaseSingle = () => http({
    url: '/purchase/total',
    method: 'POST'
})
// 单个查看和编辑采购退货信息
export const getSingleSearchEditAPI = (object) => http({
    url: '',
    method: 'POST',
    params: {
        object
    }
})
// 内容表格里审核状态改变的按钮 6.14
export const ChangeState = (id, state,username) => http({
    url: '/fixedCodeWare/updateStates',
    method: 'POST',
    params: {
        id,
        state,
        username
    }
})