import http from "@/http/notMove";

// 删除
export const delGood = (id) => http({
  url: "/goodsInbounch/del",
  method: "post",
  params: {
    id
  }
})
// http({
//   url: "/goodsInbounch/getAllList",
//   method: "get"
// })
// 获取所有商品信息
export const getAllGoodsList = async function () {
  return await http.get('/goodsInbounch/getAllList');
}