import http from '@/http/index'

// 调拨列表数据请求
export const getGoldList = (currentPage, pageSize) => http({
  url: '/allocate/list',
  method: 'GET',
  data: {
    currentPage, pageSize
  }
})

// 调拨列表总数量请求
export const getGoldTotal = () => http({
  url: '/allocate/total',
  method: 'GET'
})

//新增调拨列表数据请求
export const getNewGoldList = (currentPageSync, page) => http({
  url: '/allocate/newlist',
  method: 'GET',
  data: {
    currentPageSync, page
  }
})

//新增调拨列表总数量请求
export const getNewGoldTotal = () => http({
  url: '/allocate/newtotal',
  method: 'GET'
})