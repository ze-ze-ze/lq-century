import http from '@/http/index'

// 员工列表数据请求
export const getEmployeeListApi = (page, size) => http({
  url: '/staff/selectByPage',
  method: 'POST',
  params: {
    page,
    size
  }
})

// // 员工总数量请求
// export const getEmployeeTotalApi = () => http({
//   url: '/employee/total',
//   method: 'POST'
// })

// 增加员工数据请求
export const addEmployeeApi = (obj) => http({
  url: '/staff/insert',
  method: 'POST',
  params: obj
})

// 筛选查询数据请求
export const searchEmployeeApi = (arr) => http({
  url: '/staff/selectByLike',
  method: 'POST',
  params: arr
})

// 编辑员工数据请求
export const editEmployeeApi = (obj) => http({
  url: '/staff/update',
  method: 'POST',
  params: obj
})

// 删除员工数据请求
export const delEmployeeApi = (id) => http({
  url: '/staff/delete',
  method: 'POST',
  params: {
    id
  }
})

// 批量删除员工请求
export const delallApi = (ids) => http({
  url: '/staff/batchDelete',
  method: 'POST',
  params: {
    ids
  }
})

// 分组列表数据请求
export const getGroupListApi = (page, size) => http({
  url: '/staffGrouPing/selectByPageGp',
  method: 'POST',
  params: {
    page,
    size
  }
})

// // 分组总数量请求
// export const getGroupTotalApi = () => http({
//   url: '/group/total',
//   method: 'POST'
// })

// 分组新建请求
export const addGroupApi = (obj) => http({
  url: '/staffGrouPing/insertGp',
  method: 'POST',
  params: obj
})

// 分组编辑请求
export const editGroupApi = (obj) => http({
  url: '/staffGrouPing/updateGp',
  method: 'POST',
  params: obj
})

// 分组删除请求
export const delGroupApi = (id) => http({
  url: '/staffGrouPing/deleteGp',
  method: 'POST',
  params: {
    id
  }
})

