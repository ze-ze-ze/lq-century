import http from '@/http/index';

export const loginApi = (username,pwd) => http({
    url:'/user/login',
    method:'post',
    params:{
      username,
      pwd
    }
  })

export const logoutApi = () => http({
    url:'/user/logout',
    method:'post'
  })
 
export const getRouterApi = (params) => http({
  url:'/user/selectRoutes?integer='+params,
  method:'post',
})