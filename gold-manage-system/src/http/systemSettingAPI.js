import http from './index'
// 获取列表数据
export const getSystemList = () => http({
    url: '/coding/selectAll',
    method: 'post'
})
// 单个删除
export const systemDelbyId = (id) => http({
    url: '/coding/deleteById',
    method: 'POST',
    params: {
        id
    }
})
// 新增数据
export const addsystemMsg = (object) => http({
    url: '/coding/insert',
    method: "POST",
    params: object
})
// 单个编辑 6.14
export const singleEditApi = (coding) => http({
    url: '/coding/update',
    method: 'POST',
    params:coding
}
)