import http from '@/http/index';

// 查询总部全部
export const selectMainAll = () => http({
  url: '/kctj/selectAllZb',
  method: 'post',
})

// 查询分店全部
export const selectFenAll = () => http({
  url: '/kctj/selectAllFd',
  method: 'post',
})

// 根据id查询分店
export const selectFenById = (id) => http({
  url: '/kctj/selectByIdFd',
  method: 'post',
  params: { id }
})

// 根据id查询分店
export const selectMainById = (id) => http({
  url: '/kctj/selectByIdZb',
  method: 'post',
  params: { id }
})