import http from '@/http/index';

// 获取列表信息
export const getSupplierList = () => http({
  url: '/supplier/findByMuch',
  method: 'post',
})

// 删除
export const deleteSupplier = (id) => http({
  url: "/supplier/deleteSupById",
  method: 'post',
  params: { id }
})

// 编辑
export const editSupplier = (data) => http({
  url: "/supplier/updateSup",
  method: 'post',
  params: data
})

// 新增
export const addSupplier = (data) => http({
  url: "/supplier/insertSup",
  method: 'post',
  params: data
})