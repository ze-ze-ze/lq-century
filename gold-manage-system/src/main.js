import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import http from '@/http/index'
import '@/router/promission'
import httptxst from '@/http/notMove'

import '@/assets/scss/base.scss';
import '@/plugins/element-ui';

import "./bus";

Vue.config.productionTip = false
Vue.prototype.$http = http
Vue.prototype.$request = httptxst

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
