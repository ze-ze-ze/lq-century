import Vue from 'vue'

// export default new Vue() // 这种方式需要在使用的组件中引入才可以

Vue.prototype.$bus = new Vue(); // 将 bus 挂载到 Vue 原型上，所有 Vue 实例（组件）可以通过 this 访问
