import Vue from 'vue';

import { Container,Header,Aside,Main,Row,Col,Menu,MenuItem,Submenu,MenuItemGroup,Card,Button,Breadcrumb,BreadcrumbItem,Divider, Select, Option, Table,TableColumn, Pagination,Dialog,Form,FormItem,Input,MessageBox,Message,Image,Loading,Notification,CheckboxGroup, Checkbox,Icon,Dropdown,DropdownMenu,DropdownItem,Tag, DescriptionsItem, Descriptions,InputNumber,Popover,Radio,Upload,Tree,Cascader,Avatar,Footer   } from 'element-ui';

Vue.use(Container);
Vue.use(Radio);
Vue.use(Footer);
Vue.use(Avatar);
Vue.use(Upload);
Vue.use(Icon);
Vue.use(DropdownItem);
Vue.use(DropdownMenu);
Vue.use(Dropdown);
Vue.use(Header);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Row);
Vue.use(Col);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(Submenu);
Vue.use(MenuItemGroup);
Vue.use(Card);
Vue.use(Button);
Vue.use(Breadcrumb);
Vue.use(BreadcrumbItem);
Vue.use(Divider);
Vue.use(Select);
Vue.use(Option);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Pagination);
Vue.use(Dialog);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Image);
Vue.use(CheckboxGroup);
Vue.use(Checkbox);
Vue.use(Tag);
Vue.use(DescriptionsItem);
Vue.use(Descriptions);
Vue.use(InputNumber);
Vue.use(Popover);
Vue.use(Cascader);
Vue.use(Tree)



Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;
