export default{
  namespaced:true,
  state:{
    name: localStorage.getItem('name'),
    token: localStorage.getItem('token'),
    userImg: localStorage.getItem('userImg'),
    // routes:[{
    //   id: 1,
    //   name: 'DashBoard',
    //   code: null,
    //   description: null,
    //   url: '/',
    //   component: 'DashBoard',
    //   generatemenu: 1,
    //   sort: 0,
    //   parentId: null,
    //   permName: null,
    //   redirect: '/',
    //   title: 'admin',
    //   // icon: 'example',
    //   children: [
    //     {
    //       id: 2,
    //       name: 'GoodsSetNav',
    //       code: null,
    //       description: null,
    //       url: 'goods_set_nav',
    //       component: 'goods_set/GoodsSetNav',
    //       generatemenu: 1,
    //       sort: 0,
    //       parentId: 1,
    //       permName: null,
    //       redirect: '',
    //       title: '商品基础设置',
    //       icon: 'el-icon-menu',
    //       children: null
    //     },
    //     {
    //       id: 3,
    //       name: 'StoreInfrastructure',
    //       code: null,
    //       description: null,
    //       url: 'store',
    //       component: 'store_set/StoreInfrastructure',
    //       generatemenu: 1,
    //       sort: 0,
    //       parentId: 1,
    //       permName: null,
    //       redirect: '',
    //       title: '门店基础设置',
    //       icon: 'el-icon-menu',
    //       children: null
    //     },
    //     {
    //       id: 4,
    //       name: 'EmployeeInfo',
    //       code: null,
    //       description: null,
    //       url: 'employee_info',
    //       component: 'employee_set/EmployeeInfo',
    //       generatemenu: 1,
    //       sort: 0,
    //       parentId: 1,
    //       permName: null,
    //       redirect: '',
    //       title: '员工基础设置',
    //       icon: 'el-icon-setting',
    //       children: null
    //     },
    //     {
    //       id: 5,
    //       name: 'RightsManagementHome',
    //       code: null,
    //       description: null,
    //       url: 'rightsManagementHome',
    //       component: 'rightsManagement/RightsManagementHome',
    //       generatemenu: 1,
    //       sort: 0,
    //       parentId: 1,
    //       permName: null,
    //       redirect: '',
    //       title: '权限设置',
    //       icon: 'el-icon-setting',
    //       children: null
    //     },
    //     {
    //       id: 6,
    //       name: 'SystemSettings',
    //       code: null,
    //       description: null,
    //       url: 'SystemSettings',
    //       component: 'system_set/SystemSettings',
    //       generatemenu: 1,
    //       sort: 0,
    //       parentId: 1,
    //       permName: null,
    //       redirect: '',
    //       title: '系统设置',
    //       icon: 'el-icon-setting',
    //       children: null
    //     }
    //   ]
    // }],
    routes:JSON.parse(localStorage.getItem('routes')),
    initRoutes:[]
  },
  getters:{
  },
  mutations:{
    setUser(state,obj){
      state.name = obj.name;
      state.token = obj.token;
      state.userImg = obj.touxiang;
    },
    setInitRoutes(state,routes){
      state.initRoutes = routes
    },
    setRoutes(state,arr){
      state.routes = arr
    },
    clearInitRoutes(state){
      state.initRoutes = []
      state.routes = []
    }
  },
  actions:{
    setUser({commit},obj){
      return new Promise((resolve)=>{
      localStorage.setItem('name',obj.name);
      localStorage.setItem('userImg',obj.touxiang);
      localStorage.setItem('token',obj.token);
      commit('setUser',obj);
      resolve();
      })
    },
    setRoutes({commit},arr){
      return new Promise(resolve => {
        localStorage.setItem('routes',JSON.stringify(arr))
        commit('setRoutes',arr)
        resolve()
      })
    },
    clearUser({commit}){
      return new Promise((resolve)=>{
      localStorage.clear();
      commit('setUser',{});
      commit('clearInitRoutes')
      resolve();
      })
    }
  },
}