import Vue from 'vue'
import Vuex from 'vuex'
import userStore from './userStore'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    categoryIndex:1
  },
  getters: {
  },
  mutations: {
    setCategoryIndex(state,payload){
      state.categoryIndex = payload
    }
  },
  actions: {
  },
  modules: {
    userStore,
  }
})
