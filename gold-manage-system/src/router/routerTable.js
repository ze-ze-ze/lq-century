export const user = 
[
  {
    id: 1,
    name: 'DashBoard',
    code: null,
    description: null,
    url: '/',
    component: 'DashBoard',
    generatemenu: 1,
    sort: 0,
    parentId: null,
    permName: null,
    redirect: '/',
    title: '普通用户',
    // icon: 'example',
    children: [
      {
        id: 2,
        name: 'GoodsSetNav',
        code: null,
        description: null,
        url: 'goods_set_nav',
        component: 'goods_set/GoodsSetNav',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '商品基础设置',
        icon: 'el-icon-menu',
        children: null
      },
      {
        id: 3,
        name: 'StoreInfrastructure',
        code: null,
        description: null,
        url: 'store',
        component: 'store_set/StoreInfrastructure',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '门店基础设置',
        icon: 'el-icon-menu',
        children: null
      }
    ]
  }
]

export const admin = [
  {
    id: 1,
    name: 'DashBoard',
    code: null,
    description: null,
    url: '/',
    component: 'DashBoard',
    generatemenu: 1,
    sort: 0,
    parentId: null,
    permName: null,
    redirect: '/',
    title: 'admin',
    // icon: 'example',
    children: [
      {
        id: 2,
        name: 'GoodsSetNav',
        code: null,
        description: null,
        url: 'goods_set_nav',
        component: 'goods_set/GoodsSetNav',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '商品基础设置',
        icon: 'el-icon-menu',
        children: null
      },
      {
        id: 3,
        name: 'StoreInfrastructure',
        code: null,
        description: null,
        url: 'store',
        component: 'store_set/StoreInfrastructure',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '门店基础设置',
        icon: 'el-icon-menu',
        children: null
      },
      {
        id: 4,
        name: 'EmployeeInfo',
        code: null,
        description: null,
        url: 'employee_info',
        component: 'employee_set/EmployeeInfo',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '员工基础设置',
        icon: 'el-icon-setting',
        children: null
      },
      {
        id: 5,
        name: 'RightsManagementHome',
        code: null,
        description: null,
        url: 'rightsManagementHome',
        component: 'rightsManagement/RightsManagementHome',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '权限设置',
        icon: 'el-icon-setting',
        children: null
      },
      {
        id: 6,
        name: 'SystemSettings',
        code: null,
        description: null,
        url: 'SystemSettings',
        component: 'system_set/SystemSettings',
        generatemenu: 1,
        sort: 0,
        parentId: 1,
        permName: null,
        redirect: '',
        title: '系统设置',
        icon: 'el-icon-setting',
        children: null
      }
    ]
  }
]