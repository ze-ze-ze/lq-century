import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/DashBoard.vue'),
    children: [
      {
        path: '',
        component: () => import('@/views/header/HomeView.vue')
      },
      {
        path: 'home',
        name: 'HomeView',
        component: () => import('@/views/header/HomeView.vue')
      },
      {
        path: 'goods_manager',
        component: () => import('@/views/header/GoodsManager.vue'),
        children: [
          {
            path: '',
            name: 'goodsManagerNav',
            component: () => import('@/views/header/goodsManager/goodsManagerNav.vue'),
          },
          {
            path: 'goodsInbounch',
            name: 'GoodsInbounch',
            component: () => import('@/views/header/goodsManager/GoodsInbounch.vue'),
          },
          {
            path: 'codedIntoStorage',
            name: 'CodedIntoStorage',
            component: () => import('@/views/header/goodsManager/CodedIntoStorage.vue'),
          },
          {
            path: 'productDisassembly',
            name: 'ProductDisassembly',
            component: () => import('@/views/header/goodsManager/ProductDisassembly.vue'),
          },
          {
            path: 'returnGoods',
            name: 'ReturnGoods',
            component: () => import('@/views/header/goodsManager/ReturnGoods.vue'),
          },
          {
            path: 'inventoryStatistics',
            name: 'InventoryStatistics',
            component: () => import('@/views/header/goodsManager/InventoryStatistics.vue'),
          },
          {
            path: 'storesAllocate',
            name: 'StoresAllocate',
            component: () => import('@/views/header/goodsManager/storesAllocate/StoresAllocate.vue'),
          },
          {
            path: 'purchaseReturnsOrders',
            name: 'PurchaseReturnsOrders',
            component: () => import('@/views/header/goodsManager/PurchaseReturnsOrders.vue'),
          },
          {
            path: 'doosView',
            name: 'DoosView',
            component: () => import('@/views/header/goodsManager/DoosView.vue'),
          }
        ]
      },
      {
        path: 'supplier_management',
        name: 'SupplierManagement',
        component: () => import('@/views/aside/supplierManagement/SupplierManagement.vue')
      },
      // {
      //   path: 'goods_set_nav',
      //   name: 'GoodsSetNav',
      //   component: () => import('@/views/aside/goods_set/GoodsSetNav.vue')
      // },
      // {
      //   path: 'store',
      //   name: 'StoreInfrastructure',
      //   component: () => import('@/views/aside/store_set/StoreInfrastructure.vue')
      // },
      // {
      //   path: 'employee_info',
      //   name: 'EmployeeInfo',
      //   component: () => import('@/views/aside/employee_set/EmployeeInfo.vue'),
      // },
      // {
      //   path: 'rightsManagementHome',
      //   name: 'RightsManagementHome',
      //   component: () => import('@/views/aside/rightsManagement/RightsManagementHome.vue')
      // },
      // {
      //   path: 'SystemSettings',
      //   name: 'SystemSettings',
      //   component: () => import('@/views/aside/system_set/SystemSettings.vue')
      // }

    ]
  },
  {
    path: '/login',
    name: 'LoginView',
    component: () => import('@/views/LoginView.vue')
  },
  // {
  //   path: '*',
  //   name: '404',
  //   component: () => import('@/views/404View.vue')
  // },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
