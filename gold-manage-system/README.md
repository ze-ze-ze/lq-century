# gold-manage-system

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 商品入库
inboundOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    category: , // 商品类型
    vendor: , // 供应商
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    orderNumber: , // 订单号
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    source: , // 来源
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]


### 库存统计
goodsList: [
  {
    goodsId: , //id
    goodsName: , // 名称
    goodsNum: , // 商品数量
    goodsCode: , // 编码
    commerceNum: , // 电商款号
    brand: , // 品牌
    goldComtent: , // 含金量
    goldWeight: , // 金净重
    mainStoneHeavy: , // 主石重
    viceStoneHeavy: , // 副石重
    totalHeavy: , // 总重
    costUnitPrice: , // 成本单价
    coefficient: , // 系数
    bidPrice: , // 标签单价
    LabelTotalPrice: , // 标签总价
    salesCosts: , // 销售工费
    salesSurcharge: , // 销售附加费
    priceType: , // 价格类型
    certificateNum: , // 证书号
    ringSize: , // 手寸
    clarity: , // 净度
    color: , // 颜色
    cut: , // 切工
    fluorescence: , // 荧光
    modlNum: , // 模具号
    itemNum: , // 款号
    vendor: , // 供应商
    inboundTime: , // 入库时间
    category1: , // 分类1
    category2: , // 分类2
    alias1: , // 别名1
    alias2: , // 别名2
    quality: , // 成色
    series: , // 系列
    storeName: , // 门店名称
    counter: , // 柜台
  }
]

### 配货出库
outboundOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    outboundStore: , // 出库门店 
    outboundCounter: , // 出库柜台
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    category: , // 商品类型
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    source: , // 来源
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]

### 门店调拨
storeTransferOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    outboundStore: , // 出库门店 
    outboundCounter: , // 出库柜台
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    category: , // 商品类型
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]

### 门店退货
returnsListOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    outboundStore: , // 出库门店 
    outboundCounter: , // 出库柜台
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    category: , // 商品类型
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]

### 门店退货
returnsListOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    outboundStore: , // 出库门店 
    outboundCounter: , // 出库柜台
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    category: , // 商品类型
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]

### 固定编码入库
codedIntoStorageOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    category: , // 商品类型
    vendor: , // 供应商
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    orderNumber: , // 订单号
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    source: , // 来源
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]

### 采购退货
purchaseReturnsOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    outboundStore: , // 出库门店 
    outboundCounter: , // 出库柜台
    inboundStore: , // 入库门店
    inboundcounter: , // 入库柜台
    category: , // 商品类型
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    series: , // 系列
    LabelTotalPrice: , // 标签总价
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]

### 商品拆料
productDisassemblyOrders = [
  {
    goodsId: , // id
    status: , // 状态（0：已驳回；1：已通过；2：待审核）
    documentNum: , // 单据编号
    createDate: , // 创单时间 
    headquartersName: , // 总部名称 
    category: , // 商品类型
    count: , // 数量
    totalWeight: , // 总重量
    totalPrice: , // 总成本
    LabelTotalPrice: , // 标签总价
    createPerson: , // 创单人
    approver: , // 审批人
    approvalDate: , // 审批日期
  }
]